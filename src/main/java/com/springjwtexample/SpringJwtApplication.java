package com.springjwtexample;

import com.springjwtexample.entity.User;
import com.springjwtexample.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class SpringJwtApplication {

    @Autowired
    private UserRepository userRepository;

    @PostConstruct
    public void initUsers() {
        List<User> userList = Arrays.asList(
                new User(1, "user1", "pwd1", "email1"),
                new User(2, "user2", "pwd2", "email2"),
                new User(3, "user3", "pwd3", "email3"),
                new User(4, "user4", "pwd4", "email4"),
                new User(5, "user5", "pwd5", "email5")
        );

        userRepository.saveAll(userList);
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringJwtApplication.class, args);
    }

}
