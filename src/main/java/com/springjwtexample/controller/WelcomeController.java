package com.springjwtexample.controller;

import com.springjwtexample.entity.User;
import com.springjwtexample.payload.AuthRequest;
import com.springjwtexample.repository.UserRepository;
import com.springjwtexample.service.CustomUserDetailsService;
import com.springjwtexample.util.JwtUtil;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class WelcomeController {

    private final CustomUserDetailsService userDetailsService;
    private final JwtUtil jwtUtil;
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;

    public WelcomeController(CustomUserDetailsService userDetailsService,
                             JwtUtil jwtUtil,
                             AuthenticationManager authenticationManager, UserRepository userRepository) {
        this.userDetailsService = userDetailsService;
        this.jwtUtil = jwtUtil;
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
    }

    @GetMapping("/")
    public String welcome() {
        return "Welcome spring with jwt";
    }

    @PostMapping("/authenticate")
    public String generateToken(@RequestBody AuthRequest authRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword())
            );
        } catch (Exception e) {
            throw new UsernameNotFoundException("invalid username or password");
        }
        return jwtUtil.generateToken(authRequest.getUsername());
    }

    @GetMapping("/users")
    public List<User> users() {
        return userRepository.findAll();
    }

}
