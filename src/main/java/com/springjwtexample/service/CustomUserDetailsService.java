package com.springjwtexample.service;

import com.springjwtexample.entity.User;
import com.springjwtexample.repository.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Log4j2
@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isPresent()) {
            User userResult = user.get();
            log.info("user found {}", userResult);
            return new org.springframework.security.core.userdetails.User(
                    userResult.getUsername(),
                    userResult.getPassword(),
                    new ArrayList<>());
        } else throw new UsernameNotFoundException("user not found");

    }
}
