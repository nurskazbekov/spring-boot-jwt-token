package com.springjwtexample.payload;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ExceptionResponse {
    private String message;
    private String code;
    private LocalDateTime timestamp;
}
